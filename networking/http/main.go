package main

import (
	"github.com/gorilla/mux"
	"encoding/json"
	"net/http"
	"fmt"
	"log"
	"io/ioutil"
)

type Bank struct {
	Name string
	Blz  string
}

func startServer() *http.Server {
	r := mux.NewRouter()
	r.HandleFunc("/bundesbank/v1/banks/{id}", bankHandler)
	server := &http.Server{Addr: ":8888", Handler: r}

	go func() {
		server.ListenAndServe()
	}()

	return server
}

func bankHandler(w http.ResponseWriter, r *http.Request) {
	b := Bank{"Deutsche Bank", "20200200302"}
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, marshal(b))
}

func marshal(entities interface{}) string {
	var b []byte
	var err error
	if b, err = json.Marshal(entities); err == nil {
		return fmt.Sprintf("%s", string(b[:]))
	}
	log.Fatal(err)
	return ""
}

func main() {
	server := startServer()

	resp, err := http.Get("http://localhost:8888/bundesbank/v1/banks/2")
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(body))

	server.Shutdown(nil)
}
