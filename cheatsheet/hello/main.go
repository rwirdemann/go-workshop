package main

import (
	"fmt"
)

func main() {
	i, j := 1, 2
	x, y := swap(i, j)
	fmt.Printf("%d, %d", x, y)
}

func swap(x, y int) (int, int) {
	return y, x
}
