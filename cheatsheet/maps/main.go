package main

import (
	"fmt"
)

func main() {
	m := make(map[string]string)
	m["ralf"] = "10101003"
	v := m["ralf"]

	m2 := map[string]string{
		"atti": "276323284",
	}

	fmt.Printf("%v", m)
	fmt.Printf("%v", m2)
	fmt.Printf("%v", v)

	if v, ok := m["ralf"]; ok {
		fmt.Printf("%v", v)
	}
}
