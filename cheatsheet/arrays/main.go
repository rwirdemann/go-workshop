package main

import (
	"fmt"
)

func main() {

	// Arrays
	var a [4]int
	a1 := [...]int{1, 2, 3, 4}
	a2 := [4]int{1, 2, 3, 4}

	// Slices
	s := make([]int, 3)
	s1 := []int{5, 6, 7, 8}
	s2 := a1[1:3]
	s2 = append(s2, 4)

	fmt.Printf("%v", a)
	fmt.Printf("%v", s)
	fmt.Printf("%v", a1)
	fmt.Printf("%v", a2)
	fmt.Printf("%v", s1)
	fmt.Printf("%v", s2)
}
