package main

import (
	"regexp"
	"fmt"
)

func main() {
	expected := `{"Banks":[{"Id":0,"Blz":"10010424","Bankleitzahlfuehrend":"","Bezeichnung":"Aareal Bank","PLZ":"10666","Kurzbezeichnung":"Aareal Bank","Pan":"26910","BIC":"AARBDE5W100","Pruefzifferberechnungsmethode":"09","Datensatznummer":"004795","Aenderungskennzeichen":"U","Bankleitzahlloeschung":"0","Nachfolgebankleitzahl":"00000000"}]}`
	actualee := `{"Banks":[{"Id":0,"Blz":"10010424","Bankleitzahlfuehrend":"","Bezeichnung":"Aareal Bank","PLZ":"10666","Kurzbezeichnung":"Aareal Bank","Pan":"26910","BIC":"AARBDE5W100","Pruefzifferberechnungsmethode":"09","Datensatznummer":"004795","Aenderungskennzeichen":"U","Bankleitzahlloeschung":"0","Nachfolgebankleitzahl":"00000000"}]}`

	expected = `\{"Banks":[\{"Id":0,"Blz":"10010424","Bankleitzahlfuehrend":"",`
	actualee = `\{"Banks":[\{"Id":0,"Blz":"10010424","Bankleitzahlfuehrend":"",`

	matched, err := regexp.MatchString(expected, actualee)
	fmt.Printf("Matched: %d %v", matched, err)

}
