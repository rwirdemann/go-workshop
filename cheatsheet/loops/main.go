package main

import "fmt"

func main() {
	a := []string{"eins", "zwei", "drei"}

	for index, value := range a {
		fmt.Printf("index: %d, value: %s\n", index, value)
	}

	for _, value := range a {
		fmt.Printf("%s\n", value)
	}

	m := map[string]int {
		"ralf": 50,
		"atti": 52,
	}

	for key, value := range m {
		fmt.Printf("key: %s, value: %d\n", key, value)
	}

}