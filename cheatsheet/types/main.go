package main

import "fmt"

func main() {
	var s interface{}
	s = "hallo"

	switch s.(type) {
	case string:
		fmt.Printf("string: %s", s)
	case int:
		fmt.Printf("int: %d", s)
	}
}
