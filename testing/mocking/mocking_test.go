package mocking

import (
	"bitbucket.org/rwirdemann/go-workshop/testing/mocking/mgo"
	"testing"
)

type Query interface {
	Sort() Query
}

type Collection interface {
	Find() Query
}

type MongoQuery struct {
	Query *mgo.Query
}

type MongoCollection struct {
	Collection *mgo.Collection
}

func (q MongoQuery) Sort() Query {
	return &MongoQuery{Query: q.Query.Sort()}
}

func (c MongoCollection) Find() Query {
	q := c.Collection.Find()
	return MongoQuery{Query: q}
}

type FakeQuery struct {
	SortCount int
}

func (q *FakeQuery) Sort() Query {
	q.SortCount++
	return q
}

type FakeCollection struct {
}

func (FakeCollection) Find() Query {
	return &FakeQuery{}
}

func BuildQuery(collection Collection) Query {
	q := collection.Find()
	q.Sort()
	return q
}

func TestBuildQuery(t *testing.T) {
	q := BuildQuery(FakeCollection{})
	fq, _ := q.(*FakeQuery)
	if fq.SortCount != 1 {
		t.Fatalf("wanted %d, got %d", 1, fq.SortCount)
	}

	BuildQuery(MongoCollection{Collection: &mgo.Collection{}})
}
