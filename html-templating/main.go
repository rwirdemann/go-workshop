package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type Greeting struct {
	Message string
	Greetee string
}

func main() {
	template := template.Must(template.ParseFiles("html-templating/hello.html"))
	greeting := Greeting{Message: "Hello", Greetee: "Ralf"}
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		template.Execute(w, struct{ Greeting Greeting }{greeting})
	})

	fmt.Println("Starting web tcp")
	http.ListenAndServe(":8080", nil)
	fmt.Println("Finished...")
}
