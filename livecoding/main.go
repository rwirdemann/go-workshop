package main

import "bitbucket.org/rwirdemann/go-workshop/livecoding/domain"

func talk(a domain.Animal) {
	a.Talk()
}

func main() {
	dog := domain.Dog{Name: "Kalle"}
	talk(dog)
}
