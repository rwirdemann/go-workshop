package domain

type Animal interface {
	Talk()
}

type Dog struct {
	Name string
}

func (Dog) Talk() {

}
