package service

import (
	"encoding/json"
	"log"
	"net/http"

	"fmt"
	"strings"

	"bitbucket.org/rwirdemann/go-workshop/shop/common"
	"bitbucket.org/rwirdemann/go-workshop/shop/user/domain"
	"bitbucket.org/rwirdemann/go-workshop/shop/user/jwt"
	"github.com/rs/cors"
)

var UserHandler http.Handler

func init() {
	r := http.NewServeMux()
	r.HandleFunc("/api/login", login)
	r.HandleFunc("/api/users", users)
	r.HandleFunc("/api/users/me", me)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:8000"},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	})

	UserHandler = cors.Default().Handler(r)
	UserHandler = c.Handler(UserHandler)
	http.Handle("/", r)
}

func login(writer http.ResponseWriter, request *http.Request) {
	fmt.Println("GET /api/login")
	switch request.Method {
	case "POST":
		decoder := json.NewDecoder(request.Body)
		defer request.Body.Close()
		var user domain.User
		if err := decoder.Decode(&user); err == nil {
			if token, ok := jwt.Authenticate(user.Username, user.Password); ok {
				fmt.Fprint(writer, token)
			} else {
				writer.WriteHeader(http.StatusForbidden)
			}
		}
	}
}

func users(_ http.ResponseWriter, r *http.Request) {
	log.Println("/api/users")
	switch r.Method {
	case "POST":
		decoder := json.NewDecoder(r.Body)
		defer r.Body.Close()
		var user domain.User
		if err := decoder.Decode(&user); err == nil {
			domain.CreateUser(user.Username, user.Password)
		} else {
			log.Fatal("error creating user")
		}
	}
}

func me(w http.ResponseWriter, r *http.Request) {
	log.Println("/api/users/me")
	switch r.Method {
	case "GET":
		header := r.Header.Get("Authorization")
		log.Println("Authorization: " + header)
		token := strings.Split(header, " ")[1]
		if username, err := common.ValidateToken(token); err == nil {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			json := common.Json(domain.FindUser(username))
			fmt.Println("me: " + json)
			fmt.Fprintf(w, json)
		} else {
			w.WriteHeader(http.StatusForbidden)
		}
	}
}
