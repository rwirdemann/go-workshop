package service

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"time"

	"fmt"
	"io/ioutil"

	"bitbucket.org/rwirdemann/go-workshop/shop/common"
	"bitbucket.org/rwirdemann/go-workshop/shop/user/domain"
	"bitbucket.org/rwirdemann/go-workshop/shop/user/util"
)

func TestPostUser(t *testing.T) {
	username := fmt.Sprintf("%s%d", "user", time.Now().Unix())
	user := domain.User{Username: username, Password: "test"}
	json := common.Json(user)
	req, err := http.NewRequest("POST", "/api/users", strings.NewReader(json))
	if err != nil {
		t.FailNow()
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(users)

	count := len(domain.FindUsers())
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	if newCount := len(domain.FindUsers()); count+1 != newCount {
		t.Errorf("Number of users wrong: got %v want %v", newCount, count)
	}
}

func TestSuccessfulLoginShouldReturnJwtToken(t *testing.T) {
	user := domain.User{Username: "ralf", Password: "test1234"}
	json := common.Json(user)
	req, err := http.NewRequest("POST", "/api/login", strings.NewReader(json))
	if err != nil {
		t.FailNow()
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(login)

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	bodyBytes, _ := ioutil.ReadAll(rr.Body)
	actual := string(bodyBytes)
	util.AssertEquals(t, actual, "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InJhbGYifQ.FXdhQf8swQGEjFq3WqVSn7Skgtb70yLx-ycuqv_Ii2M")
}

func TestUnsuccessfulLogin(t *testing.T) {
	user := domain.User{Username: "ralf", Password: "test12345"}
	json := common.Json(user)
	req, err := http.NewRequest("POST", "/api/login", strings.NewReader(json))
	if err != nil {
		t.FailNow()
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(login)

	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusForbidden {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusForbidden)
	}
}

func TestGetMe(t *testing.T) {
	jwt := postLogin("ralf")
	req, _ := http.NewRequest("GET", "/api/users/me", nil)
	req.Header.Add("Authorization", "Bearer "+jwt)
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(me)
	handler.ServeHTTP(rr, req)
	util.AssertEquals(t, http.StatusOK, rr.Code)

	decoder := json.NewDecoder(rr.Body)
	var user domain.User
	if err := decoder.Decode(&user); err == nil {
		util.AssertEquals(t, "ralf", user.Username)
		util.AssertEquals(t, "Ralf", user.Firstname)
		util.AssertEquals(t, "Wirdemann", user.Lastname)
	} else {
		t.FailNow()
	}
}

func postLogin(username string) string {
	user := domain.User{Username: username, Password: "test1234"}
	json := common.Json(user)
	req, _ := http.NewRequest("POST", "/api/login", strings.NewReader(json))
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(login)
	handler.ServeHTTP(rr, req)
	bodyBytes, _ := ioutil.ReadAll(rr.Body)
	return string(bodyBytes)
}
