package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/rwirdemann/go-workshop/shop/user/service"
)

func main() {
	fmt.Println("Starting user service on port 4000...")
	http.ListenAndServe(":4000", service.UserHandler)
}
