package domain

import (
	"database/sql"
	"log"

	// Import for its side-effects: register driver
	"bitbucket.org/rwirdemann/go-workshop/shop/common"
	_ "github.com/go-sql-driver/mysql"
)

func init() {
	if common.DB == nil {
		user, password := "ralf", "test1234"
		common.Connect(user, password, "userservice")
	}
}

func FindUser(username string) *User {
	var password string
	var firstname string
	var lastname string

	err := common.DB.QueryRow("select password, firstname, lastname from users where username = ?",
		username).Scan(&password, &firstname, &lastname)
	switch {
	case err == sql.ErrNoRows:
		log.Printf("No user with name '%s'", username)
	case err != nil:
		log.Fatal(err)
	default:
		return &User{Username: username, Password: password, Firstname: firstname, Lastname: lastname}
	}

	return nil
}

func FindUsers() []*User {
	rows, err := common.DB.Query("select id, username, password from users")
	if err != nil {
		panic(err)
	}

	var users []*User
	for rows.Next() {
		var id int64
		var username string
		var password string
		if err := rows.Scan(&id, &username, &password); err != nil {
			panic(err)
		}
		users = append(users, &User{Username: username, Password: password})
	}

	return users
}

func CreateUser(username string, password string) {
	if _, err := common.DB.Exec("insert into users (username, password) VALUES (?, ?)", username, password); err != nil {
		panic(err)
	}
}
