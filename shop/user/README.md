# User Service
Benutzerverwaltung, Registrierung, Authentifzierung

## Bounded Context
User: username, password

## Authentifzierung
Die Authentifizierung erfolgt via JWS-Token. Der User Service prüft Username und PassworT und erzeugt bei Erfolg ein JWS-Token. Das Token wird mit einem allen Services bekannten Secret signiert. Clients senden das Token in jedem Request im Header:

```
Authorization: Bearer <token>
```

## Beispiel
1. Shop logged sich ein mit dem User "ralf" und bekommt als Response ein JWS-Token mit folgender Payload:

```
{
    "user": "1",     
    "username": "ralf
}
```
2. Shop fragt Catalog nach Artikelliste: GET /api/catalog/articles und sendet im Header das JWS-Token
3. Catalog überprüft Token und holt Username aus Payload
4. Catalog sendet Artikel zurück an den Shop-Client

## Exkurs JWT

Ziel: Sicherer Austausch zur Übertragung von Informationen als JSON-Objekt. Hauptanwendung: Authentifzierung

### JWT Header
```
{
    "typ": "JWT",
    "alg": "HS256" // Algorithmus, den der Sender zur Verschlüsselung des
                   // Token verwendet.
}
```

### JTW Payload
```
{
    "iss" : "http://myapi.com", // reserved claims,
    "user": "go programmer"     // public claims
}
```

Attribute, die der User selber definieren kann, z.B. Username oder Rollen.

### JWT Signature
```
HAMCSHA256(
    base64UrlEncode(header) + "." +
    base64UrlEncode(payload),
    secret)
)
```

Header und Payload werden Base64 encoded, signed mit einem Secret. Sender und Empfänger kennen das Secret, d.h. nur diese beiden können die Signatur überprüfen. Auf dieser Art wird die Identität des Senders sichergestellt. Das Base64-Encoding stellt zusätzlich sicher, dass die Nachricht nicht verändert wurde.

Das Token besteht aus drei Strings, getrennt durch Dots:
```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.        // base64 encoded Header
eyJ1c2VybmFtZSI6InJhbGYifQ.                  // base64 encode Payload
FXdhQf8swQGEjFq3WqVSn7Skgtb70yLx-ycuqv_Ii2M  // Signature 
```

### Der Client
1. Entschlüsselt die Signature und erhält so den encoded Header und Payload
2. Vergleicht Header und Payload mit den im Token enthaltenen Daten

Was wird erreicht:
- Identität des Senders ist sicher gestellt
- die Daten wurden während des Transport nicht verändert

Header und Payload sind nicht beschlüsseelt, sondern nur Base64 encoded. Angreifer können die Daten decodieren und Lesen.

#### Encryption vs. Signing
Encryption: Public Key wird zum verschlüsseln genutzt, Private key zum entschlüsseln. Hier ist die Identität des Senders quasi egal. 

Signing: Sender verschlüsselt mit seinem Private-Key, Empfänger entschlüsselt mit dem Publiy-Key des Senders. Auf dieser Art wird die Identität des Senders sichergestellt. 

JWT: Die Daten sind nur gesigned, aber nicht encryptet.