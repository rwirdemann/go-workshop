package jwt

import (
	"testing"

	"fmt"

	"bitbucket.org/rwirdemann/go-workshop/shop/common"
	"bitbucket.org/rwirdemann/go-workshop/shop/user/util"
)

func TestSuccessfullAuthentication(t *testing.T) {
	tokenString, ok := Authenticate("ralf", "test1234")
	fmt.Printf("Token: %s", tokenString)
	util.AssertTrue(t, ok)
	username, err := common.ValidateToken(tokenString)
	util.AssertNil(t, err)
	util.AssertEquals(t, "ralf", username)
}

func TestFailedAuthentication(t *testing.T) {
	tokenString, ok := Authenticate("ralf", "test234")
	util.AssertFalse(t, ok)
	util.AssertEquals(t, "", tokenString)
}

func TestTokenManupulation(t *testing.T) {
	tokenString, ok := Authenticate("ralf", "test1234")
	util.AssertTrue(t, ok)
	_, err := common.ValidateToken(tokenString + "x")
	util.AssertNotNil(t, err)
}
