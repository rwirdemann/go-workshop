package jwt

import (
	"fmt"
	"log"

	"bitbucket.org/rwirdemann/go-workshop/shop/user/domain"
	jwtgo "github.com/dgrijalva/jwt-go"
)

func Authenticate(username string, password string) (string, bool) {
	if !checkCredentials(username, password) {
		return "", false
	}

	// Build and sign JWS token
	secret := []byte("kalle4ever")
	token := jwtgo.NewWithClaims(jwtgo.SigningMethodHS256, jwtgo.MapClaims{
		"username": username,
	})

	tokenString, err := token.SignedString(secret)
	if err != nil {
		log.Println(err)
		return "", false
	}

	return tokenString, true
}

func checkCredentials(username string, password string) bool {
	fmt.Println("checkCredentials")
	if user := domain.FindUser(username); user != nil && user.Password == password {
		return true
	}
	return false
}
