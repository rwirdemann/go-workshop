package common

import "fmt"
import jwtgo "github.com/dgrijalva/jwt-go"

// ValidateToken returns the username on successful token validation
func ValidateToken(tokenString string) (string, error) {
	secret := []byte("kalle4ever")
	token, err := jwtgo.Parse(tokenString, func(token *jwtgo.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwtgo.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return secret, nil
	})

	if claims, ok := token.Claims.(jwtgo.MapClaims); ok && token.Valid {
		return claims["username"].(string), nil
	}
	return "", err
}
