package common

import (
	"database/sql"
	"fmt"
)

var DB *sql.DB

func Connect(user string, password string, databaseName string) {
	dataSourceName := fmt.Sprintf("%s:%s@/%s?parseTime=true", user, password, databaseName)
	var err error
	if DB, err = sql.Open("mysql", dataSourceName); err != nil {
		println("Error connecting database")
	}
}
