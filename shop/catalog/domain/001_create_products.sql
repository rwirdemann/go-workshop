DROP TABLE products IF EXISTS;

CREATE TABLE products (
    id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) 
);

INSERT INTO products (name) VALUES ('Schuhe');
INSERT INTO products (name) VALUES ('Uhr');
INSERT INTO products (name) VALUES ('Kite');
INSERT INTO products (name) VALUES ('Auto');