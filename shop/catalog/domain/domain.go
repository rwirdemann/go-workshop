package domain

type Product struct {
	id   int64
	name string
}
