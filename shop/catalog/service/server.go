package service

import (
	"fmt"
	"net/http"

	"github.com/rs/cors"
)

var CatalogHandler http.Handler

func init() {
	r := http.NewServeMux()
	r.HandleFunc("/api/catalog/products", products)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:8000"},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: true,
	})

	CatalogHandler = cors.Default().Handler(r)
	CatalogHandler = c.Handler(CatalogHandler)
	http.Handle("/", r)
}

func products(writer http.ResponseWriter, request *http.Request) {
	fmt.Println("GET /api/catalog/producs")
}
