package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/rwirdemann/go-workshop/shop/catalog/service"
)

func main() {
	fmt.Println("Starting catalog service on port 4001...")
	http.ListenAndServe(":4001", service.CatalogHandler)
}
