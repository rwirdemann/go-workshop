## Übersicht

![Alt text](shop-overview.png)

## Datenbanken und User

CREATE USER 'ralf'@'localhost' IDENTIFIED BY 'test1234' PASSWORD EXPIRE;
GRANT ALL ON *.* TO 'ralf'@'localhost';

CREATE DATABASE userservice;

## Services starten

### User Service

go run shop/user/main.go 

### Frontend

cd shop/frontend; python -m SimpleHTTPServer 8000