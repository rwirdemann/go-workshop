# Inventory Service
Verwaltung von Beständen

## Bounded Context
Inventory
  product_id
  in_stock

## API
GET  /inventory/PRODUCT_ID // Bestand abfragen
POST /inventory/PRODUCT_ID // Neuen Bestand anlegen 
PUT  /inventory/PRODUCT_ID // Bestand verringern oder erhöhen