# Methoden

Es ist nicht möglich Methoden auf Standardtypen, wie int oder string zu definieren. Folgends Codefragment compiliert nicht: "cannot define new methods on non-local type string".

```go
func (s string) toUpper() string {  // Compiliert nicht
	return strings.ToUpper(s)
}
```

Abhilfe bietet die Definition eines eigenen Typs auf Basis des Standardtyps:

```go
type mystring string

func (s mystring) toUpper() string {
	return strings.ToUpper(string(s))
}
```