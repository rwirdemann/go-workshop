package main

import (
	"fmt"
	"strings"
)

type mystring string

func (s mystring) toUpper() string {
	return strings.ToUpper(string(s))
}

func main() {
	var s mystring = "hello"
	fmt.Printf("%s\n", s.toUpper())
}
