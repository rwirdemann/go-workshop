package main

import "fmt"

func main() {
	m := make(map[string]string)

	// Lesezugriff auf ein nicht vorhandenen Wert liefert default
	fmt.Printf("m[\"ralf\"]: \"%s\"\n", m["ralf"])
}
