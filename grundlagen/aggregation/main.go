package main

import "fmt"

type Vehicle interface {
	drive()
}

// Motor implementiert Vehicle explizit
type Motor struct {
	PS int
}

func (a Motor) drive() {
	fmt.Printf("Hey, Vehicle'm Motor\n")
}

// Bus implementiert Vehicle implizit durch Aggregation
type Bus struct {
	Motor
}

// Truck implementiert Vehicle implizit durch Aggregation von Motor, überschreibt
// aber die von Motor implementierte Methode "drive"
type Truck struct {
	Motor
}

func (c Truck) drive() {
	fmt.Printf("Hey, Vehicle'm Truck\n")
}

func start(v Vehicle) {
	v.drive()
}

func main() {
	start(Bus{Motor{100}})
	start(Truck{})
}
