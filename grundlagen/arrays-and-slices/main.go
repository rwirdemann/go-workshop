package main

import "fmt"

func main() {
	slices()
	appendToSlice()
}

func arrays() {
	var a [3]int

	for i, v := range a {
		fmt.Printf("i: %d, v: %d", i, v)
	}

	b := [3]int{1, 2, 3}
	for i, v := range b {
		fmt.Printf("i: %d, v: %d", i, v)
	}
	println()
}

func slices() {
	s := []int{2}
	fmt.Printf("%v\n", s)
}

type A struct {
	s []string
}

func appendToSlice() {
	a := A{}
	a.s = append(a.s, "hallo")
	fmt.Printf("%v\n", a.s)
}
