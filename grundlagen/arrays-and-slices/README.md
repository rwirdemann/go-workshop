# Arrays and Slices

Arrays sind Listen gleichen Typs mit fester Länge:

```go
a := [3]int{1, 2, 3}
```

Fehlt die explizite Initialisierung, werden die Elemente mit ihrem Default-Wert, im Beispiel mit 0 initialisiert:

```go
var b [3]int
```

Der Typ eines Arrays besteht aus seinem Elementtyp sowie aus seiner Länge

```go
c := [4]int{1, 2, 3, 4}
b = a // ok
b = c // Compiler-Fehler, [3]int != 4[int]
```

Zuweisung von Arrays erzeugt Kopien

## Slices

Slices referenzieren Arrays und verhalten sich wie Pointer. Ändert eine Funktion Elemente auf einem als Parameter übergebene Slice, dann sind die Änderungen vom Aufrufer sichtbar.

Slices haben eine Länge, die sich Ändern kann, solange das unterliegende Arrays es hergibt



